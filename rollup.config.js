import babel from 'rollup-plugin-babel';

// rollup.config.js
export default {
  entry: `lib/revit.js`,
  format: `cjs`,
  plugins: [babel()],
  dest: `bin/revit.js` // equivalent to --output
};
