const test = require(`tape`);

test(`---- revit: Basic Test ----`, (swear) => {
  const
    swearJar = 1,
    bonafiedMsg = `Hello revit`,
    imagineMsg = `Hello revit`;

  swear.plan(swearJar);
  swear.equal(bonafiedMsg, imagineMsg, `Expected msg to say hi`);
});
